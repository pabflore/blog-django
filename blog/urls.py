from django.urls import path
from . import views

urlpatterns = [
    path('framework/crear/',views.crear_framework,name="crear_framework"),
    path('frameworks/',views.lista_framework,name="lista_framework"),
    path('frameworks/<int:pk>',views.DetalleFramework.as_view(),name="detalle_framework"),
    path('frameworks/<int:pk>/editar',views.EditarFramework.as_view(),name="editar_framework"),
    path('frameworks/<int:pk>/borrar',views.BorrarFramework.as_view(),name="borrar_framework"),
]