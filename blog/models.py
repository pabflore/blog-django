from django.db import models
from ckeditor.fields import RichTextField


class Framework(models.Model):
    framework = models.CharField(max_length=30,null=True) #Nombre del framework
    lenguaje = models.CharField(max_length=30) #lenguaje de programacion
    orientacion =  models.CharField(max_length=30) #Lenguaje orientado a [BackEnd, FrontEnd]
    autor = models.CharField(max_length=30) #empresa o persona que creo el Framework
    tipo = models.CharField(max_length=50) #interpretado o compilado
    descripcion = RichTextField(blank=True, null=True)
    fecha = models.DateField(null=True,blank=True)
    imagen = models.ImageField(upload_to='frameworkImg', blank=True, null=True)

    def __str__(self):
        return f"Framework: {self.framework} - {self.lenguaje} - Autor: {self.autor} [Leer más]"
