from unittest.util import _MAX_LENGTH
from django import forms
from ckeditor.fields import RichTextFormField


class CrearFramework(forms.Form):
    framework =forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Framework de programación',"class":"form-control"}))
    lenguaje =forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Lenguaje escrito',"class":"form-control"}))
    orientacion = forms.CharField(max_length=30,  widget=forms.TextInput(attrs={'placeholder': 'Orientado a Back o FrontEnd',"class":"form-control"}))
    autor = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Auto del posteo',"class":"form-control"}))
    tipo = forms.CharField(max_length=30, required=False,  widget=forms.TextInput(attrs={'placeholder': 'Tipo interpretado o compilado',"class":"form-control"}))
    descripcion = RichTextFormField(required=False)
    fecha = forms.DateField(required=False,widget=forms.TextInput(attrs={'placeholder': '2022-01-01'}))
    imagen = forms.ImageField(required=False)

class FrameworkBuscar(forms.Form):
    framework = forms.CharField(max_length=30,widget=forms.TextInput(attrs={'placeholder': 'Ingrese el framework a buscar',"class":"form-control"}))
