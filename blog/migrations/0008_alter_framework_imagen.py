# Generated by Django 4.0.3 on 2022-04-20 01:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_delete_enlaces_delete_post_framework_fecha_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='framework',
            name='imagen',
            field=models.ImageField(blank=True, null=True, upload_to='frameworkImg'),
        ),
    ]
