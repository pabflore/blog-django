from django.shortcuts import redirect, render
from .models import Framework
from .forms import CrearFramework, FrameworkBuscar
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your views here.


@login_required #este es un decorador
def crear_framework(request):
    if request.method == 'POST':
        formulario = CrearFramework(request.POST, request.FILES)
        if formulario.is_valid():
            data = formulario.cleaned_data
            framework = Framework(framework=data['framework'],
                lenguaje=data['lenguaje'],
                orientacion=data['orientacion'],
                autor=data['autor'], tipo=data['tipo'],
                descripcion=data['descripcion'],
                fecha = data['fecha'],
                imagen = data['imagen']
             )
            framework.save()
            return redirect('/pages/frameworks/') 
    form = CrearFramework()
    return render(request, 'blog/crear_framework.html', {'form': form})



def lista_framework(request):
    framework_buscar=[]
    dato = request.GET.get('framework',None)
    if dato is not None:
        framework_buscar = Framework.objects.filter(framework__icontains=dato)
    else:
        framework_buscar = Framework.objects.all()
    buscador = FrameworkBuscar()
    return render(
        request, 'blog/lista_framework.html', 
        {'buscador': buscador, 'framework_buscar':framework_buscar,'dato': dato})


#en clases basadas en vistas no se usan los decoradores, se usan los mixins
class DetalleFramework(DetailView):
    model = Framework
    template_name = 'blog/detalle_framework.html'

class EditarFramework(LoginRequiredMixin, UpdateView):
    model = Framework
    success_url = '/pages/frameworks/'
    fields = ['framework','lenguaje', 'orientacion', 'autor', 'tipo','descripcion','fecha','imagen' ]

class BorrarFramework(LoginRequiredMixin, DeleteView):
    model = Framework
    success_url = '/pages/frameworks/'
    