from django.shortcuts import render
# Create your views here.


def inicio(request):
    return render(request, "indice/index.html", {})


def contacto(request):
    return render(request, "indice/contacto.html", {})


def about(request):
    return render(request, "indice/quienes_somos.html")


