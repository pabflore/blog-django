from django.urls import path
from .views import inicio,contacto,about

urlpatterns = [
    path('', inicio,name='indice'),
    path('contacto/', contacto,name='contacto'),
    path('about/', about,name='quienes_somos'),
]