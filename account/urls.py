from django.urls import path
from .views import editar_usuario, mi_login,registro
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('login/', mi_login,name='login'),
    path('logout/', LogoutView.as_view(template_name='account/logout.html'), name='logout'),
    path('registro/', registro,name='registro'),
    path('editar/', editar_usuario,name='editar_usuario'),
]