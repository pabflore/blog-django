from os import link
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserExtension(models.Model):
    avatar = models.ImageField(upload_to='avatar', blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    link = models.URLField(max_length=100,null=True, blank=True)
    more_description = models.CharField(max_length=30)