from django.shortcuts import render,redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, authenticate

from account.models import UserExtension
from .forms import NuestroUserForm, EditFullUser
from django.contrib.auth.decorators import login_required
from .models import UserExtension

def mi_login(request):
    msj=''
    if request.method == 'POST':
        login_form = AuthenticationForm(request,data=request.POST)
        if login_form.is_valid():
            username = login_form.cleaned_data['username']
            passowrd = login_form.cleaned_data['password']
            user = authenticate(username=username, password=passowrd)
            if user is not None:
                login(request, user)
                return render(request, "indice/index.html", {})
            else:
                msj='El usuario no se pudo autenticar.'
        else:
            msj='El formulario no es valido.'
    login_form = AuthenticationForm()
    return render(request, 'account/login.html',{'login_form': login_form,'msj':msj})


def registro(request):
    if request.method == 'POST':
        form = NuestroUserForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            form.save()
            return redirect('login')
        else:
            return render(request, 'account/registro.html',{'form':form, 'msj':'El formulario no es valido'})        

    form = NuestroUserForm()
    return render(request, 'account/registro.html',{'form':form})

@login_required
def editar_usuario(request):
    user_extension_logued, _ = UserExtension.objects.get_or_create(user=request.user)
    if request.method == 'POST':
        form = EditFullUser(request.POST, request.FILES)

        if form.is_valid():
            request.user.email = form.cleaned_data['email']
            request.user.first_name = form.cleaned_data['first_name']
            request.user.last_name = form.cleaned_data['last_name']

            user_extension_logued.link = form.cleaned_data['link']
            user_extension_logued.more_description = form.cleaned_data['more_description']
            user_extension_logued.avatar = form.cleaned_data['avatar']

            if form.cleaned_data['password1'] !='' and form.cleaned_data['password1'] == form.cleaned_data['password2']:
                request.user.set_password(form.cleaned_data['password1'])

            request.user.save()
            user_extension_logued.save()

            return redirect('indice')
        else:
            return render(request, 'account/editar_usuario.html',{'form':form, 'msj':'El formulario no es valido'})        

    form = EditFullUser(
        initial={
            'email': request.user.email,
            'password1':'',
            'password2':'',
            'first_name': request.user.first_name,
            'last_name': request.user.last_name, 
            'avatar': user_extension_logued.avatar,
            'link': user_extension_logued.link, 
            'more_description': user_extension_logued.more_description
        }
    )
    try:
        return render(request, 'account/editar_usuario.html',{'form':form, 'user_avatar_url': buscar_url_avatar(request.user)})
    except:
        return render(request, 'account/editar_usuario.html',{'form':form})


def buscar_url_avatar(user):
    return UserExtension.objects.filter(user=user)[0].avatar.url