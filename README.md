# Este es un blog 

Este es un blog creado en Django para rendir como trabajo final del curso de Python. Tiene por finalidad documentar sobre los distintos Framework de desarrollo que existen hoy en día.

# Instalación

Como primer paso es necesario que crees tu entorno virtual con una versión de python 3.7 o superior de la siguiente manera
```bash
python3.9 -m venv venv
```

Para cargar el entorno virtual debes ejecutar el siguiente comando (Ejemplo en Linux)
```bash
. venv/bin/activate
```

Luego que el entorno virtual esta cargado debes instalar los requerimientos de la siguiente forma
```python
pip install -r requirements.txt
```

En la raiz del proyecto es necesario crear el archivo .env y crear la variable de entorno y su llave
SECRET_KEY='tu_llave'


# Ejecutar APP
Para arrancar la aplicación debes ejecutar el siguiente comando:
```python
python manage.py runserver
```

Una vez arrancado el servicio dirigete a tu navegador web y abre la URL 
[http://127.0.0.1:8000/](http://127.0.0.1:8000/)


# Primeros pasos

Una vez abierto el proyecto en su navegador, verá una imagen como la siguiente.


<p align="center">

## Página principal
  <img src="img/1.png" width="750" title="hover text">

## Menú del sitio
Haga clic en MENU para desplegar
  <img src="img/2.png" width="750" alt="accessibility text">

## Login
Para loguarse al sistema utilice las siguientes credenciales:
Usuario: admin
Contraseña: admin123456

  <img src="img/3.png" width="750" alt="accessibility text">
Ingrese el autor, título y mail del posteo y presione "Enviar"

## Crear nuevo Framework
Para agregar un nuevo Framwork haga clic sobre "Agregar Framework" he ingrese todos los campos requeridos.
  <img src="img/4.png" width="750" alt="accessibility text">
Una ves ingresados los campos presione el botón "Enviar para almacenar en la Base de Datos"

## Crear nuevo enlace
Por último, para agregar enlaces de interes, link documentos PDF, etc. Debe ingresar el lenguague en el cual está escrito el Framework, titulo del documento o link, enlace y presione "Enviar para almacenar en la BD." 
  <img src="img/5.png" width="750" alt="accessibility text">
</p>

## Que hizo cada uno
<strong>Pablo Flores:</strong>
Creación de de app blog y acount, creación de modelos vistas y urls de ambas aplicaiones.

<strong>Nicolas Palassoli:</strong>
Eleccion de templete, estructura del sitio, creacion de modelos, account


## Video de la presentacion lo puede encontrar en el directorio /img/PresentacionTrabajoFinal.mkv
